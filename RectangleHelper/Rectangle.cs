﻿namespace RectangleHelper;

public class Rectangle
{    
    public static int GetArea(int firstSideRectangle, int secondSideRectangle)
    {
        return (firstSideRectangle * secondSideRectangle);
    }

    public static int GetPerimeter(int firstSideRectangle, int secondSideRectangle)
    {
        return (2 * (firstSideRectangle + secondSideRectangle));
    }
}
