﻿namespace ArrayHelper;

public class Array
{
    public static void BubbleSort(int[] arrayToSort)
    {
        for (int i = 0; i <= arrayToSort.Length - 1; ++i)
        {
            for (var j = 0; j < arrayToSort.Length - 1 - i; ++j)
            {
                if (arrayToSort[j] > arrayToSort[j + 1])
                {
                    Swap(ref arrayToSort[j], ref arrayToSort[j + 1]);
                }
            }           
        }

    }

    private static void Swap(ref int x, ref int y)
    {
        var temp = x;
        x = y;
        y = temp;
    }
}
