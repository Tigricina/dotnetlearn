﻿using System;
using ArrayHelper;
using RectangleHelper;

class Program
{
    static void Main(string[] args)
    {
        var inputArray = new[] { 10, 9, 50, 3, 2, 6, 15, 11 };
        int firstSideRectangleRefresh = 5;
        int secondSideRectangleRefresh = 10;

        ArrayHelper.Array.BubbleSort(inputArray);
        var scuare = RectangleHelper.Rectangle.GetArea(firstSideRectangleRefresh, secondSideRectangleRefresh);
        var perimetr = RectangleHelper.Rectangle.GetPerimeter(firstSideRectangleRefresh, secondSideRectangleRefresh);
	
	    Console.WriteLine($"This is area of rectangle: {scuare}");
	    Console.WriteLine($"This is perimetr of rectangle: {perimetr}");
        Console.WriteLine("Массив после сортировки:");
        for (int i = 0; i < inputArray.Length; i++)
        {
            Console.Write(inputArray[i] + " ");
        }
    }
}

